from sklearn import svm
from utils.preprocess_data import get_train_features
import numpy as np
from sklearn import tree
import pickle
from sklearn.model_selection import cross_val_score

#to face class imbalance
def oversample(x,y):
    n = 0
    for each in y:
        if each == 1:
            n+=1
    m = len(y) - 2*n

    for i in range(m):
        ind = np.random.randint(0,len(y)-1)
        while(y[ind]!= 1):
            ind = np.random.randint(0, len(y) - 1)
        x=np.vstack([x, [x[ind]]])
        y=np.append(y, [y[ind]])
    return x,y

#get training features
x,dt, y = get_train_features()

clfTree = tree.DecisionTreeClassifier()

clfTree = clfTree.fit(dt, y)

x,y = oversample(x,y)
clfSVM = svm.LinearSVC()

clfSVM.fit(x, map(float, y))

#Save trained models
pickle.dump(clfTree,open("dTmodel.pkl", 'wb'))
pickle.dump(clfSVM,open("SVMmodel.pkl", 'wb'))


