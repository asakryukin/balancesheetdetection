import pickle
class Classifier:

    def __init__(self):
        self.clfTree, self.clfSVM = self.load_models()


    def load_models(self):
        return pickle.load(open("model/dTmodel.pkl", 'rb')), pickle.load(open("model/SVMmodel.pkl", 'rb'))

    def classify(self, data):
        #dt_feature, sheet_idf = process_text(text)

        Tp = self.clfTree.predict(data[0])
        Sp = self.clfSVM.predict(data[1])

        print("tree prediction:" + str(Tp))
        print("Sp prediction:" + str(Sp))

        return 0.5*(Tp+Sp)