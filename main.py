# -*- coding: utf-8 -*-
from model.Classifier import Classifier
import pandas as pd
from utils.DataHandler import DataHandler
import sys

reload(sys)
sys.setdefaultencoding('utf8')

Cl = Classifier()
# #
# # df = pd.read_excel('data/Dataset.xlsx')
#
# res = Cl.classify(df['sheet_text'][227])

string = "→ TABLE OF CONTENTS\n01. Chairman's letter\n02 CEO's report\n03 Directors' report\n04 Auditor's independence declaration\n05. Corporate governance statement\n06 Statement of profit or loss and\nother comprehensive income\nStatement of financial position\n08 Statement of changes in equity\n09 Statement of cash flows\n10 Notes to the financial statements\n11 Directors' declaration\nIndependent auditor's report to the\nmembers of iSentia Group Limited\n13 Shareholder information\n12\n01\nCHAIRMAN'S\nLETTER\niSentia Group Limited – Annual Report 2014\nChairman's Letter 1\n"
data = DataHandler.get_model_features(string)#'data/MoreData.xlsx')
res = Cl.classify(data)
print res
