import nltk
nltk.download('punkt')

stop_symbols = [':',"-",",",".","?",'(',')','\n']
important_pairs = [['total', 'assets'], ['total','liabilities'], ['total', 'equity'],['stockholders','equity'],['shareholders','equity'],['current', 'assets'],['current', 'liabilities']]

def tokenize_text(text):
    text = text.encode('utf8')
    text = text.replace("'","")
    text = remove_stop_symbols(text)
    text = text.lower()
    text = split_currs(text,abbrs, symbs)
    words = nltk.word_tokenize(text)

    dict = {}
    #total word count
    total_wcount = 0
    #numbers
    total_ncount = 0
    #currency symbols and abbreviations
    total_acount = 0

    for w in words:
        if(check_number(w)):

            total_ncount += 1
        elif(check_abbr(w,abbrs, symbs)):
            total_acount += 1
        else:
            if(dict.has_key(w)):
                dict[w] += 1
            else:
                dict[w] = 1
            total_wcount += 1

    #detect important pairs
    imp_pairs = check_imp_pairs(words)
    return dict,total_wcount, total_ncount, total_acount, imp_pairs, len(dict.keys())

#check if string is a number
def check_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def remove_stop_symbols(s):

    for symb in stop_symbols:
        s = s.replace(symb," ")
    return s

#load lists of currencies
def load_currency():
    res = []
    with open("data/abbrvs.csv",'r') as file:
        for line in file:
            line = line.replace('\n','')
            res.append(line.lower())
    symbs = []
    with open("data/symbols.csv", 'r') as file:
        for line in file:
            line = line.replace('\n', '')
            symbs.append(line.lower())

    return res, symbs

def split_currs(s,abrs, symbs):

    for a in abrs:
        s = s.replace(a," "+a+" ")
    for a in symbs:
        s = s.replace(a," "+a+" ")

    return s

#check if contains currencies
def check_abbr(s, abrs, symbs):
    if(s in abrs):
        return True
    if (s in symbs):
        return True

    return False

abbrs, symbs = load_currency()

#check for important pairs
def check_imp_pairs(w):
    t = 0
    for i in range(len(w)-1):
        if([w[i],w[i+1]] in important_pairs):
            t+=1

    return t