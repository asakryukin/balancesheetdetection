import numpy as np



def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def get_train_features():
    sheet_vectors = []
    labels = []
    dt_features = []

    #iterate through excel rows
    for index, row in df.iterrows():
        dt_feature, sheet_idf = process_text(row['sheet_text'])

        dt_features.append(dt_feature)
        sheet_vectors.append(sheet_idf)
        labels.append(int(row['is_balance_sheet']))

    return np.array(sheet_vectors, np.float32), dt_features, np.array(labels, np.float32)
