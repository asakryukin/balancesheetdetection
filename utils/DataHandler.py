import pandas as pd
from tokenize_text import tokenize_text
import numpy as np

class DataHandler:
    # 20 most frequent words for balance sheets
    dictionary = ['reported', 'adjusted', 'and', 'liabilities', 'assets', 'total', 'months', 'current', 'term', 'other',
                  'unnamed', 'trade', 'three', 'stockholders', 'reserve', 'million', 'long', 'in', 'fasb', 'equity']

    @staticmethod
    def extractExcel(filename):
        assert filename[-5:] == ".xlsx"
        #'data/Dataset.xlsx'
        df = pd.read_excel(filename)
        result = []
        for index, row in df.iterrows():
            result.append(row['sheet_text'])

        return result

    @staticmethod
    def convertInput(inputObject):
        if(type(inputObject) is str and inputObject[-5:] == ".xlsx"):
            return DataHandler.extractExcel(inputObject)
        elif(isinstance(inputObject, str)):
            return [inputObject]
        elif(type(inputObject) is list):
            return inputObject

    # create word vector
    @staticmethod
    def create_feature_vector(d):
        res = []
        for word in DataHandler.dictionary:
            if (d.has_key(word)):
                res.append(d[word])
            else:
                res.append(0)

        res = np.array(res, np.float32)
        if (sum(res) > 0):
            res = res / sum(res)
        return res

    @staticmethod
    def process_text(text):
        res, twc, tnc, ac, ip, dc = tokenize_text(text)

        dt_feature = [float(tnc) / twc, ip, float(dc) / twc]
        sheet_idf = DataHandler.create_feature_vector(res)

        return dt_feature, sheet_idf


    @staticmethod
    def get_model_features(inputObject):

        text_collection = DataHandler.convertInput(inputObject)

        dt_features = []
        sheet_vectors = []

        for text in text_collection:
            dtf, sv = DataHandler.process_text(text)
            dt_features.append(dtf)
            sheet_vectors.append(sv)

        return dt_features, sheet_vectors
